<?php


class Database
{
    protected  $conn ;
 public function __construct()
 {
     require_once "config.php";
     $conf = new config();
     $this ->conn  = new  mysqli($conf->Servername,$conf->username,$conf->password,$conf->dbname);
     $this->name =null;
     $this->values=null;
     $this->keys=null;
     $this->query=null;
     $this->q=null;
     if ($this ->conn->connect_error)
         return(false);
     else
         return(true);
 }

 public function set_name($name)
 {
     $this->name = $name;
     return $this;
 }
 public function set_values()
 {
     $this->values = func_get_args();
     return $this;
 }
 public function set_keys()
 {   $this->keys = func_get_args();
     return $this;
 }
 public function where($where)
 {
     $this->query .="WHERE ". $where;
     return $this;
 }
 public function select()
 {
     $select='';
    if ($this->keys!=null) {
        foreach ($this->keys as $key)
            $select .= ' ' . $key . ',';
        $select = substr($select,0,count($select)-2);
    }
    else $select=' *';

    $this->query= "SELECT$select FROM $this->name ";
    $this->q ="select";
    return $this;
 }
 public function insert()
 {
     $values = str_replace(['[',']',"\\"],['(',')',"\\\\"],json_encode($this->values));
     $keys = str_replace(['[',']','"'],['(',')',''],json_encode($this->keys));
     if(gettype($this->values[0])=='array')
     $values = substr($values,2,strlen($values)-4);
     $this->query = "INSERT INTO  `$this->name` $keys VALUES $values";
     return $this;
 }
 public function array_To_Value($array)
 {
     $s = json_encode($array);
     $s[0]='(';
     $s[strlen($s)-2]=')';
 }

 public function update()
 {
     $update ="";
     for($i=0;$i<count($this->keys);$i++) {
         $key = $this->keys[$i];
         $value = $this->values[$i];
         $update .= "`$key`='$value', ";
     }
     $update = substr($update,0,count($update)-3);
     $this->query = "UPDATE $this->name SET $update";
    return $this;

 }
 public function run()
 {
     if($this->q=="select")
     {
         $result = $this->conn->query($this->query);
         if ($result->num_rows > 0) {
             $this->clear();
             return ($result->fetch_array());
         }
         else {
             $this->clear();
             return (false);
         }
     }
     if($this->conn->query($this->query)) {
         $this->clear();
         return true;
     }
     else {
         $this->clear();
         return $this->conn->error;
     }
 }
 protected function clear()
{
    $this->name =null;
    $this->values=null;
    $this->keys=null;
    $this->query=null;
    $this->q=null;
}
}