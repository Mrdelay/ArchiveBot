<?php

require_once ("config.php");
class bot
{

 protected  $token , $url , $curl;
 public $msg, $uid, $chat, $msgid, $data,$json,$callback_id;
 public function __construct()
 {
     $conf = new config();
     $json = file_get_contents('php://input');
     $telegram = urldecode ($json);
     $results = json_decode($telegram);
        $this->json = $results;
        $this->token = $conf->token;
        $this->url = "https://api.telegram.org/bot".$this->token."/";
        $this->curl = [array(),$this->url];
        if(isset($this->json->message))
            $this->Get_message();
        else if (isset($this->json->callback_query))
            $this->Get_callback();
     unset($conf,$json,$telegram,$results);
 }
 protected function Get_message()
 {
     $message = $this->json->message;
     $this->msg = $message->text ;
     $this->uid = $message->chat->id;
     $this->chat = $message->chat;
     $this->msgid=$message->message_id;
 }
 protected function Get_callback()
 {
     $callback = $this->json->callback_query;
     $this->data = $callback->data;
     $this->callback_id = $callback->id;
     $message = $callback->message;
     $this->msg = $message->text ;
     $this->uid = $message->chat->id;
     $this->chat = $message->chat;
     $this->msgid= $message->message_id;
 }
 public function send($msg,$id=0,$parse_mode=null)
 {
     $id = $id?$id:$this->uid;
     return file_get_contents($this->url."sendMessage?chat_id=$id&text=$msg&parse_mode=$parse_mode");
 }
 public function update($msg,$msg_id,$id=0,$parse_mode=null)
 {
     $id = $id?$id:$this->uid;
     return file_get_contents($this->url."editMessageText?chat_id=$id&message_id=$msg_id&text=$msg&parse_mode=$parse_mode");
 }
 public function photo($adress,$id=0,$reply_to_message_id=0)
 {
     $this->curl[0]['photo'] = new CURLFile(realpath($adress));
     $this->curl[0]['chat_id'] = $id?$id:$this->uid;
     if($reply_to_message_id)
         $this->curl[0]['reply_to_message_id']=$reply_to_message_id;
     $this->curl[1] .= 'sendPhoto?';
     return $this;
 }
 public function answerCallbackQuery($show_alert =false,$url=null,$cache_time=0)
 {
     $this->curl[0]['callback_query_id'] = $this->callback_id;
     $this->curl[0]['show_alert']=$show_alert;
     $this->curl[0]['cache_time']=$cache_time;
     if($url != null)
         $this->curl[0]['url']=$url;
     $this->curl[1] .= 'answerCallbackQuery?';
     return $this;
 }
 public function text($text,$parse_mode=null)
 {
    $this->curl[0]['text']=$text;
     if($parse_mode!=null)
         $this->curl[0]['parse_mode']=$parse_mode;
    return $this;
 }
 public function caption($caption,$parse_mode=null)
 {
     $this->curl[0]['caption']=$caption;
     if($parse_mode!=null)
         $this->curl[0]['parse_mode']=$parse_mode;
     return $this;
 }
 public function reply_markup($reply_markup,$text=null,$caption=false)
 {
     $this->curl[0]['reply_markup']=$reply_markup;
     if($text==null)
         return $this;
     if($caption)
         $this->curl[0]['caption']=$text;
     else
         $this->curl[0]['text']=$text;
     return $this;
 }
 public function run($disable_notification=0)
 {
    if($disable_notification)
        $this->curl[0]['disable_notification']= $disable_notification;
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
         "Content-Type:multipart/form-data"
     ));
     curl_setopt($ch, CURLOPT_URL, $this->curl[1]);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $this->curl[0]);
     return curl_exec($ch);
 }

}
class Replay
{
    public function __construct($width=1,$resize=false,$one=false,$selectiv=false)
    {
        $this->keyboard = array();
        $this->row=array();
        $this->lo = 0;
        $this->w=$width;
        $this->re=$resize;
        $this->on=$one;
        $this->sel=$selectiv;
    }
    public function add($text,$contact=false,$location=false)
    {
        array_push($this->row,array("text"=>$text,"request_contact"=>$contact,"request_location"=>$location));
        $this->lo++;
        if($this->lo==$this->w)
        {
            $this->lo=0;
            array_push($this->keyboard,$this->row);
            $this->row=array();
        }

    }
    public function output()
    {
        if (count($this->row)>0)
            array_push($this->keyboard,$this->row);
        return "&reply_markup=".json_encode(array("resize_keyboard"=>$this->re,
                "one_time_keyboard"=>$this->on,"selective"=>$this->sel,"keyboard"=>$this->keyboard));

    }
}
class Inline
{
    public function __construct($width=1)
    {

        $this->keyboard = array();
        $this->row=array();
        $this->button=array();
        $this->lo = 0;
        $this->w=$width;
    }
    public function text($text)
    {
        $this->button['text']=$text;
        return $this;
    }
    public function url($url)
    {
        $this->button['url']=$url;
        return $this;
    }
    public function callback($callback)
    {
        $this->button['callback_data']=$callback;
        return $this;
    }
    public function switchiq($data)
    {
        $this->button['switch_inline_query']=$data;
        return $this;
    }
    public function switchiqcc($data)
    {
        $this->button['switch_inline_query_current_chat']=$data;
        return $this;
    }
    public function callbackgame($data)
    {
        $this->button['callback_game']=$data;
        return $this;
    }
    public function pay($data)
    {
        $this->button['pay']=$data;
        return $this;
    }
    public function add()
    {
        array_push($this->row,$this->button);
        $this->lo++;
        $this->button=array();
        if($this->lo==$this->w)
        {
            $this->lo=0;
            array_push($this->keyboard,$this->row);
            $this->row=array();
        }

    }
    public function output()
    {
        if (count($this->row)>0)
            array_push($this->keyboard,$this->row);
        return "&reply_markup=".json_encode(array("inline_keyboard"=>$this->keyboard));

    }
    public function output_json()
    {
        if (count($this->row)>0)
            array_push($this->keyboard,$this->row);
        return json_encode(array("inline_keyboard"=>$this->keyboard));
    }
}
