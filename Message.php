<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 2/9/19
 * Time: 12:02 PM
 */
require_once ("bot.php");


class Message
{
    public function welcome()
    {
        $text = "خوش آمدید";
        $keyboard = new Replay(2);
        $keyboard->add("جست و جو");
        $keyboard->add("لیست دسته بندی ها");
        return $text.$keyboard->output();
    }
    public function Category($list)
    {
        $text = "لیست دسته بندی ها"."%0A";
        $keyboard = new Inline(3);
        foreach ($list as $key =>$row) {
            $text .= '-' . $row . "%0A";
            $keyboard->text($row)->callback("c*$key")->add();
        }
        return $text.$keyboard->output();


    }
    public function game()
    {
        $text = "دسته بندی ها";
        $keyboard = new Inline(3);
        $keyboard->text("اکشن")->url("http://google.com")->add();
        $keyboard->text("فانتزی")->callback("test1")->add();
        $keyboard->text("تست")->url("http://p30download.com")->add();
        return $text.$keyboard->output();
    }
    public function profile($data)
    {
        $status = (int)$data['status'];
        $phone='___';
        $email='___';
        $user ='___';
        $lastname = $data['lname'] != null?$data['lname']:"___";
        $lan = $data['lancode'];
        $name = $data['name'];
        $status = $status>=256?$status-256:$status;
        if($status >=128)
        {
            $phone = $data['phoneC'].$data['phone'];
            $status -= 128;
        }
        if($status >=64)
        {
            $user = $data['username'];
            $status -= 64;
        }
        if($status >=32)
        {
            $email = $data['email'];
            $status -= 32;
        }
        $text=
            "<pre>email:    $email

 
phone:    $phone


username: $user


name:     $name


lastname: $lastname


language: $lan 

</pre>";
        $text = str_replace("
",'%0A',$text);
        $keyboard = new Inline(3);
        $keyboard->text('email')->callback('profile*change_email')->add();
        $keyboard->text('phone')->callback('profile*change_phone')->add();
        $keyboard->text('username')->callback('profile*change_username')->add();
        $keyboard->text('name')->callback('profile*change_name')->add();
        $keyboard->text('lastname')->callback('profile*change_lastname')->add();
        $keyboard->text('language')->callback('profile*change_language')->add();
        return $text.$keyboard->output();
    }


    public function profile_action($action,&$status,$query)
    {
        $data = explode('_',$action);
        $change = array("name"=>["نام",4],
            "email"=>["ایمیل",1],
            "phone"=>["شماره موبایل",2],
            "username"=>["نام کاربری",3],
            "lastname"=>["نام خانوادگی",5]);
        if($action!='back_profile') {
            $text = $change[$data[1]][0] . " خودر را وارد کنید";
            $keyboard = new Inline(1);
            $keyboard->text('بازگشت')->callback("profile*back_profile")->add();
            $status = $change[$data[1]][1];
            return $text . $keyboard->output();
        }
        $status=0;
        return $this->profile($query);



    }
    public function email_msg($link)
    {
        $email="ایمیل تایید هویت".$link;
        return $email;
    }
    public  function games($arra)
    {
        $text = "name:".$arra['name'];
        $text .="%0Atype:".$arra['type'];
        $keyboard = new Inline(3);
        $keyboard->text("قبلی")->callback("game*back_".$arra['id'])->add();
        $keyboard->text("اجرای بازی")->callback("game*start_".$arra['id'])->add();
        $keyboard->text("بعدی")->callback("game*next_".$arra['id'])->add();
        return [$keyboard->output_json(),$text];
    }

}